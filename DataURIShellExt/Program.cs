﻿/**
 * Licensed under The MIT License
 */

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Text;
using DataURIShellExt.Properties;
using System.Globalization;

namespace DataURIShellExt
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            string[] args = System.Environment.GetCommandLineArgs();
            if ( args.Length == 2 )
            {
                // File path and extension.
                string path = args[1];
                string ext = Path.GetExtension(path).ToLower();

                // Extension to MIME type map.
                Dictionary<string, string> mimeTypes = new Dictionary<string, string>();
                mimeTypes[".jpg"] = "image/jpeg";
                mimeTypes[".jpeg"] = "image/jpeg";
                mimeTypes[".gif"] = "image/gif";
                mimeTypes[".png"] = "image/png";

                // Get MIME type.
                string mimeType = "UNKNOWN";
                if (mimeTypes.ContainsKey(ext))
                {
                    mimeType = mimeTypes[ext];
                }

                // Open image binary.
                FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                int size = (int)fs.Length;

                // Avoid over 1MB.
                if (size >= 1024 * 1024)
                {
                    MessageBox.Show(Resources.msgOver1MB, "DataURIShellExt", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                // Close
                byte[] buffer = new byte[size];
                fs.Read(buffer, 0, size);
                fs.Close();

                // Create Data URI schema and copy it to clipboard.
                string data_uri = "data:" + mimeType + ";base64," + Convert.ToBase64String(buffer);
                Clipboard.SetText(data_uri);
            }
        }

    }
}
