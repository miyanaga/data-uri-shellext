

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>
  ideamans / fastpage / source &mdash; Bitbucket
</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <!--[if lt IE 9]>
  <script src="https://dwz7u9t8u8usb.cloudfront.net/m/73ec23b97f39/js/lib/html5.js"></script>
  <![endif]-->

  <script>
    (function (window) {
      // prevent stray occurrences of `console.log` from causing errors in IE
      var console = window.console || (window.console = {});
      console.log || (console.log = function () {});

      var BB = window.BB || (window.BB = {});
      BB.debug = false;
      BB.cname = false;
      BB.CANON_URL = 'https://bitbucket.org';
      BB.MEDIA_URL = 'https://dwz7u9t8u8usb.cloudfront.net/m/73ec23b97f39/';
      BB.images = {
        noAvatar: 'https://dwz7u9t8u8usb.cloudfront.net/m/73ec23b97f39/img/no_avatar.png'
      };
      BB.user = {
        isKbdShortcutsEnabled: true,
        isSshEnabled: false
      };
      BB.user.has = (function () {
        var betaFeatures = [];
        betaFeatures.push('repo2');
        return function (feature) {
          return _.contains(betaFeatures, feature);
        };
      }());
      BB.repo || (BB.repo = {});
  
      BB.user.follows = {
        repos: '468584,468566,554799,563840,483462,583892'.split(',')
      };
      BB.user.id = 232024;
    
      BB.user.username = 'miyanaga';
    
    
    
  
  
      BB.user.isAdmin = true;
      BB.repo.id = 468584;
    
    
      BB.repo.language = 'php';
      BB.repo.pygmentsLanguage = 'php';
    
    
      BB.repo.slug = 'fastpage';
    
    
      BB.repo.owner = {
        username: 'ideamans'
      };
    
      // Coerce `BB.repo` to a string to get
      // "davidchambers/mango" or whatever.
      BB.repo.toString = function () {
        return BB.cname ? this.slug : this.owner.username + '/' + this.slug;
      }
    
    
      BB.changeset = 'c18ef6868b30'
    
    
  
    }(this));
  </script>

  


  <link rel="stylesheet" href="https://dwz7u9t8u8usb.cloudfront.net/m/73ec23b97f39/bun/css/bundle.css"/>



  <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="Bitbucket" />
  <link rel="icon" href="https://dwz7u9t8u8usb.cloudfront.net/m/73ec23b97f39/img/logo_new.png" type="image/png" />
  <link type="text/plain" rel="author" href="/humans.txt" />


  
    <script src="https://dwz7u9t8u8usb.cloudfront.net/m/73ec23b97f39/bun/js/bundle.js"></script>
  



</head>

<body id="" class="">
  <script>
    if (navigator.userAgent.indexOf(' AppleWebKit/') === -1) {
      $('body').addClass('non-webkit')
    }
    $('body')
      .addClass($.client.os.toLowerCase())
      .addClass($.client.browser.toLowerCase())
  </script>
  <!--[if IE 8]>
  <script>jQuery(document.body).addClass('ie8')</script>
  <![endif]-->
  <!--[if IE 9]>
  <script>jQuery(document.body).addClass('ie9')</script>
  <![endif]-->

  <div id="wrapper">



  <div id="header-wrap">
    <div id="header">
    <ul id="global-nav">
      <li><a class="home" href="http://www.atlassian.com">Atlassian Home</a></li>
      <li><a class="docs" href="http://confluence.atlassian.com/display/BITBUCKET">Documentation</a></li>
      <li><a class="support" href="/support">Support</a></li>
      <li><a class="blog" href="http://blog.bitbucket.org">Blog</a></li>
      <li><a class="forums" href="http://groups.google.com/group/bitbucket-users">Forums</a></li>
    </ul>
    <a href="/" id="logo">Bitbucket by Atlassian</a>

    <div id="main-nav">
    

      <ul class="clearfix">
        <li><a href="/explore" id="explore-link">Explore</a></li>
        <li><a href="https://bitbucket.org" id="dashboard-link">Dashboard</a></li>
        <li id="repositories-dropdown" class="inertial-hover active">
          <a class="drop-arrow" href="/repo/mine" id="repositories-link">Repositories</a>
          <div>
            <div>
              <div id="repo-overview"></div>
              <div class="group">
                <a href="/repo/create" class="new-repository" id="create-repo-link">Create repository</a>
                <a href="/repo/import" class="import-repository" id="import-repo-link">Import repository</a>
              </div>
            </div>
          </div>
        </li>
        <li id="user-dropdown" class="inertial-hover">
          <a class="drop-arrow" href="/miyanaga">
            <span>miyanaga</span>
          </a>
          <div>
            <div>
              <div class="group">
                <a href="/account/" id="account-link">Account</a>
                <a href="/account/notifications/" id="inbox-link">Inbox <span>(2)</span></a>
              </div>
              <div class="group">
                <a href="/account/signout/">Log out</a>
              </div>
            </div>
          </div>
        </li>
        

<li class="search-box">
  
    <form action="/repo/all">
      <input type="search" results="5" autosave="bitbucket-explore-search"
             name="name" id="searchbox"
             placeholder="owner/repo" />
  
  </form>
</li>

      </ul>

    
    </div>
    </div>
  </div>

    <div id="header-messages">
  
  
    
    
    
    
  

    
   </div>



    <div id="content">
      <div id="source">
      
  
  





  <script>
    jQuery(function ($) {
        var cookie = $.cookie,
            cookieOptions, date,
            $content = $('#content'),
            $pane = $('#what-is-bitbucket'),
            $hide = $pane.find('[href="#hide"]').css('display', 'block').hide();

        date = new Date();
        date.setTime(date.getTime() + 365 * 24 * 60 * 60 * 1000);
        cookieOptions = { path: '/', expires: date };

        if (cookie('toggle_status') == 'hide') $content.addClass('repo-desc-hidden');

        $('#toggle-repo-content').click(function (event) {
            event.preventDefault();
            $content.toggleClass('repo-desc-hidden');
            cookie('toggle_status', cookie('toggle_status') == 'show' ? 'hide' : 'show', cookieOptions);
        });

        if (!cookie('hide_intro_message')) $pane.show();

        $hide.click(function (event) {
            event.preventDefault();
            cookie('hide_intro_message', true, cookieOptions);
            $pane.slideUp('slow');
        });

        $pane.hover(
            function () { $hide.fadeIn('fast'); },
            function () { $hide.fadeOut('fast'); });

      (function () {
        // Update "recently-viewed-repos" cookie for
        // the "repositories" drop-down.
        var
          id = BB.repo.id,
          cookieName = 'recently-viewed-repos_' + BB.user.id,
          rvr = cookie(cookieName),
          ids = rvr? rvr.split(','): [],
          idx = _.indexOf(ids, '' + id);

        // Remove `id` from `ids` if present.
        if (~idx) ids.splice(idx, 1);

        cookie(
          cookieName,
          // Insert `id` as the first item, then call
          // `join` on the resulting array to produce
          // something like "114694,27542,89002,84570".
          [id].concat(ids.slice(0, 4)).join(),
          {path: '/', expires: 1e6} // "never" expires
        );
      }());
    });
  </script>




<div id="tabs">
  <ul class="tabs">
    <li>
      <a href="/ideamans/fastpage/overview" id="repo-overview-link">Overview</a>
    </li>

    <li>
      <a href="/ideamans/fastpage/downloads" id="repo-downloads-link">Downloads (<span id="downloads-count">0</span>)</a>
    </li>

    

    <li>
      <a href="/ideamans/fastpage/pull-requests" id="repo-pr-link">Pull requests (0)</a>
    </li>

    <li class="selected">
      
        <a href="/ideamans/fastpage/src" id="repo-source-link">Source</a>
      
    </li>

    <li>
      <a href="/ideamans/fastpage/changesets" id="repo-commits-link">Commits</a>
    </li>

    <li id="wiki-tab" class="dropdown"
      style="display:
          block 
        
      ">
      <a href="/ideamans/fastpage/wiki" id="repo-wiki-link">Wiki</a>
    </li>

    <li id="issues-tab" class="dropdown inertial-hover"
      style="display:
        block 
        
      ">
      <a href="/ideamans/fastpage/issues?status=new&amp;status=open" id="repo-issues-link">Issues (0) &raquo;</a>
      <ul>
        <li><a href="/ideamans/fastpage/issues/new">Create new issue</a></li>
        <li><a href="/ideamans/fastpage/issues?status=new">New issues</a></li>
        <li><a href="/ideamans/fastpage/issues?status=new&amp;status=open">Open issues</a></li>
        <li><a href="/ideamans/fastpage/issues?status=duplicate&amp;status=invalid&amp;status=resolved&amp;status=wontfix">Closed issues</a></li>
        
          <li><a href="/ideamans/fastpage/issues?responsible=miyanaga">My issues</a></li>
        
        <li><a href="/ideamans/fastpage/issues">All issues</a></li>
        <li><a href="/ideamans/fastpage/issues/query">Advanced query</a></li>
      </ul>
    </li>

    
      <li>
        <a href="/ideamans/fastpage/admin" id="repo-admin-link">Admin</a>
      </li>
    

    <li class="secondary">
      <a href="/ideamans/fastpage/descendants" id="repo-forks-link">Forks/queues (0)</a>
    </li>

    <li class="secondary">
      <a href="/ideamans/fastpage/zealots">Followers (<span id="followers-count">3</span>)</a>
    </li>
  </ul>
</div>




  <div id="invitation-dialog" title="Send an invitation">

<form class="invitation-form newform"
  method="post"
  action="/api/1.0/invitations/ideamans/fastpage"
  novalidate>
  <div style='display:none'><input type='hidden' name='csrfmiddlewaretoken' value='a292d8ecf198c88700be181e6e187fb0' /></div>
  <div class="error_ message_"><h4></h4></div>
  <div class="success_ message_"><h4></h4></div>
  <label for="id-email-address">Email address</label>
  <input type="email" id="id-email-address" name="email-address">
  <select name="permission" class="nosearch">
  
    <option value="write">Write access</option>
    <option value="admin">Admin access</option>
  </select>
  <input type="submit" value="Send invitation" />
</form>
</div>
 

  <div class="repo-menu" id="repo-menu">
    <ul id="repo-menu-links">
    
      <li>
        <a href="#share" class="share">invite</a>
      </li>
    
      <li>
        <a href="/ideamans/fastpage/rss?token=dac4503484bf719699a5346436c969f0" class="rss" title="RSS feed for fastpage">RSS</a>
      </li>

      <li><a href="/ideamans/fastpage/fork" class="fork">fork</a></li>
      
      <li>
        <a rel="nofollow" href="/ideamans/fastpage/follow" class="follow following">following</a>
      </li>
      
        <li>
          <a href="/xhr/ideamans/fastpage/revoke" class="revoke" title="revoke your admin access to this repository">revoke</a>
        </li>
        
      
        <li class="get-source inertial-hover">
          <a class="source">get source</a>
          <ul class="downloads">
            
              
              <li><a rel="nofollow" href="/ideamans/fastpage/get/c18ef6868b30.zip">zip</a></li>
              <li><a rel="nofollow" href="/ideamans/fastpage/get/c18ef6868b30.tar.gz">gz</a></li>
              <li><a rel="nofollow" href="/ideamans/fastpage/get/c18ef6868b30.tar.bz2">bz2</a></li>
            
          </ul>
        </li>
      
    </ul>

  
    <ul class="metadata">
      
      
        <li class="branches inertial-hover">branches
          <ul>
            <li><a href="/ideamans/fastpage/src/c18ef6868b30">master</a>
              
              
            </li>
            <li><a href="/ideamans/fastpage/src/4b206a8540a9">miyanga-dev</a>
              
              
              <a rel="nofollow" class="menu-compare"
                 href="/ideamans/fastpage/compare/miyanga-dev..master"
                 title="Show changes between miyanga-dev and the main branch.">compare</a>
              
            </li>
          </ul>
        </li>
      
      
      <li class="tags inertial-hover">tags
        <ul>
          <li><a href="/ideamans/fastpage/src/51ba92576951">1.0.0-RC4</a>
            
            
              <a rel="nofollow" class='menu-compare'
                 href="/ideamans/fastpage/compare/..1.0.0-RC4"
                 title="Show changes between 1.0.0-RC4 and the main branch.">compare</a>
            </li>
          <li><a href="/ideamans/fastpage/src/a793c06748f6">1.0.0-RC1</a>
            
            
              <a rel="nofollow" class='menu-compare'
                 href="/ideamans/fastpage/compare/..1.0.0-RC1"
                 title="Show changes between 1.0.0-RC1 and the main branch.">compare</a>
            </li>
        </ul>
      </li>
     
     
    </ul>
  
</div>

<div class="repo-menu" id="repo-desc">
    <ul id="repo-menu-links-mini">
      

      
        <li>
          <a href="#share" class="share" title="Invite">invite</a>
        </li>
      
      <li>
        <a href="/ideamans/fastpage/rss?token=dac4503484bf719699a5346436c969f0" class="rss" title="RSS feed for fastpage"></a>
      </li>

      <li><a href="/ideamans/fastpage/fork" class="fork" title="Fork"></a></li>
      
      <li>
        <a rel="nofollow" href="/ideamans/fastpage/follow" class="follow following">following</a>
      </li>
      
        <li>
          <a href="/xhr/ideamans/fastpage/revoke" class="revoke" title="revoke your admin access to this repository">revoke</a>
        </li>
        
      
        <li>
          <a class="source" title="Get source"></a>
          <ul class="downloads">
            
              
              <li><a rel="nofollow" href="/ideamans/fastpage/get/c18ef6868b30.zip">zip</a></li>
              <li><a rel="nofollow" href="/ideamans/fastpage/get/c18ef6868b30.tar.gz">gz</a></li>
              <li><a rel="nofollow" href="/ideamans/fastpage/get/c18ef6868b30.tar.bz2">bz2</a></li>
            
          </ul>
        </li>
      
    </ul>

    <h3 id="repo-heading" class="public git">
      <a class="owner-username" href="/ideamans">ideamans</a> /
      <a class="repo-name" href="/ideamans/fastpage/wiki/Home">fastpage</a>
    

    
      <ul id="fork-actions" class="button-group">
      
      
        <li>
          <a href="/ideamans/fastpage/pull-request/new"
             class="icon pull-request">create pull request</a>
        </li>
      
      </ul>
    
    </h3>

    
      <p class="repo-desc-description">FastPage is a library to accelerate web page loading.</p>
    

  <div id="repo-desc-cloneinfo">Clone this repository (size: 1.1 MB):
    <a href="https://bitbucket.org/ideamans/fastpage.git" class="https">HTTPS</a> /
    <a href="ssh://git@bitbucket.org/ideamans/fastpage.git" class="ssh">SSH</a>
    <pre id="clone-url-https">git clone https://bitbucket.org/ideamans/fastpage.git</pre>
    <pre id="clone-url-ssh">git clone git@bitbucket.org:ideamans/fastpage.git</pre>
    
      <img src="https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2012/Jan/21/fastpage-logo-955446817-1_avatar.png" class="repo-avatar" />
    
  </div>

        <a href="#" id="toggle-repo-content"></a>

        

</div>




      
  <div id="source-container">
    

  <div id="source-path">
    <h1>
      <a href="/ideamans/fastpage/src" class="src-pjax">fastpage</a> /

  
    
      <span>MIT-LICENSE.txt</span>
    
  

    </h1>
  </div>

  <div class="labels labels-csv">
  
    <dl>
  
    
  
  
    
  
  
    <dt>Branch</dt>
    
      
        <dd class="branch unabridged"><a href="/ideamans/fastpage/changesets/tip/master" title="master">master</a></dd>
      
    
  
</dl>

  
  </div>


  
  <div id="source-view">
    <div class="header">
      <ul class="metadata">
        <li><code>c18ef6868b30</code></li>
        
          
            <li>20 loc</li>
          
        
        <li>1.1 KB</li>
      </ul>
      <ul class="source-view-links">
        
        <li><a id="embed-link" href="https://bitbucket.org/ideamans/fastpage/src/c18ef6868b30/MIT-LICENSE.txt?embed=t">embed</a></li>
        
        <li><a href="/ideamans/fastpage/history/MIT-LICENSE.txt">history</a></li>
        
        <li><a href="/ideamans/fastpage/annotate/c18ef6868b30/MIT-LICENSE.txt">annotate</a></li>
        
        <li><a href="/ideamans/fastpage/raw/c18ef6868b30/MIT-LICENSE.txt">raw</a></li>
        <li>
          <form action="/ideamans/fastpage/diff/MIT-LICENSE.txt" class="source-view-form">
          
            <input type="hidden" name="diff2" value="c18ef6868b30" />
            <select name="diff1">
            
              
            
            </select>
            <input type="submit" value="diff" />
          
          </form>
        </li>
      </ul>
    </div>
  
    <div>
    <table class="highlighttable"><tr><td class="linenos"><div class="linenodiv"><pre><a href="#cl-1"> 1</a>
<a href="#cl-2"> 2</a>
<a href="#cl-3"> 3</a>
<a href="#cl-4"> 4</a>
<a href="#cl-5"> 5</a>
<a href="#cl-6"> 6</a>
<a href="#cl-7"> 7</a>
<a href="#cl-8"> 8</a>
<a href="#cl-9"> 9</a>
<a href="#cl-10">10</a>
<a href="#cl-11">11</a>
<a href="#cl-12">12</a>
<a href="#cl-13">13</a>
<a href="#cl-14">14</a>
<a href="#cl-15">15</a>
<a href="#cl-16">16</a>
<a href="#cl-17">17</a>
<a href="#cl-18">18</a>
<a href="#cl-19">19</a>
<a href="#cl-20">20</a>
</pre></div></td><td class="code"><div class="highlight"><pre><a name="cl-1"></a>Copyright (c) 2011 ideaman&#39;s Inc., http://www.ideamans.com/
<a name="cl-2"></a>
<a name="cl-3"></a>Permission is hereby granted, free of charge, to any person obtaining
<a name="cl-4"></a>a copy of this software and associated documentation files (the
<a name="cl-5"></a>&quot;Software&quot;), to deal in the Software without restriction, including
<a name="cl-6"></a>without limitation the rights to use, copy, modify, merge, publish,
<a name="cl-7"></a>distribute, sublicense, and/or sell copies of the Software, and to
<a name="cl-8"></a>permit persons to whom the Software is furnished to do so, subject to
<a name="cl-9"></a>the following conditions:
<a name="cl-10"></a>
<a name="cl-11"></a>The above copyright notice and this permission notice shall be
<a name="cl-12"></a>included in all copies or substantial portions of the Software.
<a name="cl-13"></a>
<a name="cl-14"></a>THE SOFTWARE IS PROVIDED &quot;AS IS&quot;, WITHOUT WARRANTY OF ANY KIND,
<a name="cl-15"></a>EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
<a name="cl-16"></a>MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
<a name="cl-17"></a>NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
<a name="cl-18"></a>LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
<a name="cl-19"></a>OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
<a name="cl-20"></a>WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
</pre></div>
</td></tr></table>
    </div>
  
  </div>
  


  <div id="mask"><div></div></div>

  </div>

      </div>
    </div>

  </div>

  <div id="footer">
    <ul id="footer-nav">
      <li>Copyright © 2012 <a href="http://atlassian.com">Atlassian</a></li>
      <li><a href="http://www.atlassian.com/hosted/terms.jsp">Terms of Service</a></li>
      <li><a href="http://www.atlassian.com/about/privacy.jsp">Privacy</a></li>
      <li><a href="//bitbucket.org/site/master/issues/new">Report a Bug to Bitbucket</a></li>
      <li><a href="http://confluence.atlassian.com/x/IYBGDQ">API</a></li>
      <li><a href="http://status.bitbucket.org/">Server Status</a></li>
    </ul>
    <ul id="social-nav">
      <li class="blog"><a href="http://blog.bitbucket.org">Bitbucket Blog</a></li>
      <li class="twitter"><a href="http://www.twitter.com/bitbucket">Twitter</a></li>
    </ul>
    <h5>We run</h5>
    <ul id="technologies">
      <li><a href="http://www.djangoproject.com/">Django 1.3.1</a></li>
      <li><a href="//bitbucket.org/jespern/django-piston/">Piston 0.3dev</a></li>
      <li><a href="http://git-scm.com/">Git 1.7.6</a></li>
      <li><a href="http://www.selenic.com/mercurial/">Hg 1.9.2</a></li>
      <li><a href="http://www.python.org">Python 2.7.2</a></li>
      <li>bfb7e0264b62 | bitbucket05</li>
    </ul>
  </div>

  <script src="https://dwz7u9t8u8usb.cloudfront.net/m/73ec23b97f39/js/lib/global.js"></script>






  <script>
    BB.gaqPush(['_trackPageview']);
  
    BB.gaqPush(['atl._trackPageview']);

    

    

    (function () {
        var ga = document.createElement('script');
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        ga.setAttribute('async', 'true');
        document.documentElement.firstChild.appendChild(ga);
    }());
  </script>

</body>
</html>
